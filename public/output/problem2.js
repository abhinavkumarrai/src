const csv2json = require('csvtojson');
const filePath = "matches.csv";

csv2json()
  .fromFile(filePath)
  .then(json => {
    let result = {};

    json.forEach(e => {
      if (result[e.season] == null) {
        result[e.season] = {};
      }
      if (result[e.season][e.team1] == null) {
        result[e.season][e.team1] = 1;
      } else {
        result[e.season][e.team1] = result[e.season][e.team1] + 1;
      }
      if (result[e.season][e.team2] == null) {
        result[e.season][e.team2] = 1;
      } else {
        result[e.season][e.team2] = result[e.season][e.team2] + 1;
      }
    });
    console.log(result);
  })