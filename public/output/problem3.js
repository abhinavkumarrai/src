const csv2json = require('csvtojson');
const MatchPath = "matches.csv";
const DeliveriesPath = "deliveries.csv";


csv2json()
  .fromFile(MatchPath)
  .then(match => {
    let yearWithId = {};
    match.forEach(e => {
      yearWithId[e.id] = e.season;
    });
    csv2json()
      .fromFile(DeliveriesPath)
      .then(deliveries => {
        let result = {};
        let MatchOf2016 = deliveries.filter(e => yearWithId[e.match_id] == '2016');
        MatchOf2016.forEach(e => {
          if (result[e.bowling_team] == null) {
            result[e.bowling_team] = 0;
          }
          result[e.bowling_team] = result[e.bowling_team] + parseInt(e.extra_runs);
        });
        console.log(result);
      })
  });