const csv2json = require('csvtojson');
const filePath = "matches.csv";

csv2json()
  .fromFile(filePath)
  .then(json => {
    let TossAndMatchWinner = json.filter(e => e.toss_winner == e.winner);
    // console.log(TossAndMatchWinner.length);
    let result = {};
    TossAndMatchWinner.forEach(e => {
      if (result[e.toss_winner] == null) {
        result[e.toss_winner] = 1;
      } else {
        result[e.toss_winner] = result[e.toss_winner] + 1;
      }
    });
    console.log(result);

  })